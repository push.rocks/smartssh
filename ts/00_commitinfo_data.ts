/**
 * autocreated commitinfo by @pushrocks/commitinfo
 */
export const commitinfo = {
  name: '@pushrocks/smartssh',
  version: '2.0.0',
  description: 'setups SSH quickly and in a painless manner'
}
